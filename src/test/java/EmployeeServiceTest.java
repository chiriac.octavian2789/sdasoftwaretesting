import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class EmployeeServiceTest {

    @InjectMocks
    public EmployeeService employeeService;

    @Mock
    public EmployeeRepo employeeRepo;

    @Test
    public void findByNameShouldReturnEmployee() {
        //Given
       when(employeeRepo.findByName("Toni")).
               thenReturn(new Employee("Toni", "a@com", 23, 2000));
       //when
        Employee employee = employeeService.findByName("Toni");
        //then
        verify(employeeRepo).findByName("Toni");
        assertEquals("Toni", employee.name);
    }


}
