import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;

public class EmployeeTest {

    @Test
    public void setSalaryShouldThrowExeption() {
        //given
        Employee sergiu = new Employee();
        //when
        try {
            sergiu.setSalary(-1000);
            Assertions.fail("No exception");
        }
        //then
        catch (IllegalArgumentException e) {
            e.getMessage();
            assertThat(e.getMessage().equals("Salary cannot be negative!"));
        }
    }

    @ParameterizedTest
    @ValueSource(ints = {-100, -20, -700})
    public void setSalaryShouldThrowExeption2(int a) {
        // given
        Employee rafa = new Employee();
        // when
        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class,
                () -> rafa.setSalary(a));
        // then
        assertThat(illegalArgumentException.getMessage().equals("Salary cannot be negative!"));
    }


}
