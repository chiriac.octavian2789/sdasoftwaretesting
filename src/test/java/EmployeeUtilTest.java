import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class EmployeeUtilTest {

    @ParameterizedTest
    @CsvSource({"67,true","55,false"})
    public void isRetiredShouldBeSuccessful(int age, boolean isRetired) {
        //Given
        EmployeeUtil employeeUtil = new EmployeeUtil();
        //When
        boolean result = employeeUtil.isRetired(age);
        //Then
        assertEquals(isRetired, result);
    }

    public static Stream<Arguments> provideIsRetiredArgs() {
        return Stream.of(Arguments.of(67, true),
                Arguments.of(49, false));
    }

    @ParameterizedTest
    @MethodSource("provideIsRetiredArgs")
    public void isRetiredShouldBeSuccessfulPartTwo(int age, boolean isRetired) {
        //Given
        EmployeeUtil employeeUtil = new EmployeeUtil();
        //When
        boolean result = employeeUtil.isRetired(age);
        //Then
        assertEquals(isRetired, result);
    }
}
