public class EmployeeUtil {
    public static final double EURO = 4.8;

    public boolean isRetired(int age) {
        if (age < 65) {
            System.out.println("Not retired");
            return false;
        } else {
            System.out.println("Retired");
            return true;
        }
    }

    public double convertSalaryToEuro(int salaryRON) {
        return salaryRON * EURO;
    }
}
