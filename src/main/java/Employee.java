public class Employee {
    String name;
    String email;
    Integer age;
    Integer salary;

    public Employee() {
    }

    public Employee(String name, String email, Integer age, Integer salary) {
        this.name = name;
        this.email = email;
        this.age = age;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        if(salary<0){
            throw new IllegalArgumentException("Salary cannot be negative!");
        };
        this.salary = salary;
    }
}
