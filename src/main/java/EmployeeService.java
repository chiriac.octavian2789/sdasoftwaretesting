public class EmployeeService  {
    public EmployeeRepo employeeRepo;

    public EmployeeService(EmployeeRepo employeeRepo) {
        this.employeeRepo = employeeRepo;
    }

    public Employee findByName(String name) {
        return employeeRepo.findByName(name);
    }
}
